package ru.dexsys.task.controller;

import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import ru.dexsys.task.entity.Pokemon;
import ru.dexsys.task.service.PokemonService;
import ru.dexsys.task.service.UserService;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping("/pokemonList")
@AllArgsConstructor
public class PokemonRestController {

    @Autowired
    private PokemonService pokemonService;

    @Autowired
    UserService userService;

    @GetMapping
    public ModelAndView getPokemonList() {
        return new ModelAndView("PokemonList", "PokemonList", pokemonService.getPokemonList(20));
    }

    @PostMapping(params = "buttonPrevious")
    public ModelAndView buttonPreviousIsPressed(HttpServletRequest request) {
        int pageNumber = Integer.parseInt(request.getParameterValues("pokemonId")[19]) / 20 - 2;
        return new ModelAndView("PokemonList", "PokemonList", pokemonService.getPokemonList(pageNumber * 20));
    }

    @PostMapping(params = "buttonNext")
    public ModelAndView buttonNextIsPressed(HttpServletRequest request) {
        int pageNumber = Integer.parseInt(request.getParameterValues("pokemonId")[19]) / 20;
        List<Pokemon> pokemonList = pokemonService.getPokemonList(pageNumber * 20);
        return new ModelAndView("PokemonList", "PokemonList", pokemonList);
    }

}