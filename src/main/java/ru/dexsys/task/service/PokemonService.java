package ru.dexsys.task.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.dexsys.task.entity.Pokemon;
import ru.dexsys.task.gateway.PokemonGateway;
import ru.dexsys.task.repository.PokemonDao;
import java.util.List;

@Service
public class PokemonService {

    @Autowired
    private PokemonGateway pokemonGateway;

    @Autowired
    private PokemonDao pokemonDao;

    public List<Pokemon> getPokemonList(int offset){
        List<Pokemon> pokemonList = pokemonGateway.getPokemonList(offset).getBody().getResults();
        for (Pokemon pokemon : pokemonList) {
            pokemon.setId(Integer.parseInt(pokemon.getUrl().split("/")[6]));
        }

//        Pokemon pokemon = new Pokemon();
//        pokemon.setId(1);
//        pokemon.setName("Asdf");
//        pokemon.setUrl("http://localhost:8080:pokemonList");
//        pokemonDao.save(pokemon);

        return pokemonList;
    }

}