package ru.dexsys.task.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.dexsys.task.entity.User;
import ru.dexsys.task.repository.UserDao;

@Service
public class UserService {

    @Autowired
    private UserDao userDao;

    public User getUser(String name) {
        return userDao.findByName(name);
    }
}
