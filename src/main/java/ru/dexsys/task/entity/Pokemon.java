package ru.dexsys.task.entity;

import lombok.*;
import javax.persistence.*;

@Data
@Entity
@Table(name = "pokemons", schema = "public")
public class Pokemon {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "name")
    private String name;

    @Column(name = "url")
    private String url;
}