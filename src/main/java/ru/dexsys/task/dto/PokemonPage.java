package ru.dexsys.task.dto;

import lombok.Data;
import ru.dexsys.task.entity.Pokemon;

import java.util.List;

@Data
//@Entity
public class PokemonPage {
    private int count;
    private String next;
    private String previous;
    private List<Pokemon> results;
}
